<!DOCTYPE html>
<html lang="" xml:lang="">
  <head>
    <title>What next? Reproducibility in research</title>
    <meta charset="utf-8" />
    <script src="libs/header-attrs/header-attrs.js"></script>
    <link rel="stylesheet" href="xaringan-themer.css" type="text/css" />
  </head>
  <body>
    <textarea id="source">
class: center, middle, inverse, title-slide

.title[
# What next? Reproducibility in research
]
.subtitle[
## Doing it and learning from other researchers
]

---


class: middle

# First thing: *True* reproducibility is basically impossible



## Better: "reproducibility at dissemination / archiving" or "inspectability" &lt;a name=cite-Gil2016&gt;&lt;/a&gt;[[4](https://doi.org/10.1002/2015ea000136)]

???

Ask if learners can put their laptop lids down, so you have their full attention.

The subtitle is a bit sarcastically written for reasons I'll get to in a bit.

True reproducibility means being able to re-create the results on *any* computer
at any point in the near future. But with different software versions,
unmaintained or difficult to install dependencies, constant updates, differences
between operating systems, and data sharing (which is a bit of a joke in health
research). Reproducibility quickly becomes impossible. Hence why focusing on
being reproducible at the time of publication or at the very least focus on
ensuring your data analysis is "inspectable", meaning that someone can come and
read your code and understand the logic of what you are doing, even if they
can't directly reproduce the results.


---

class: middle

# Code sharing is abysmal across health sciences &lt;a name=cite-Rauh2019&gt;&lt;/a&gt;&lt;a name=cite-Evans2019&gt;&lt;/a&gt;&lt;a name=cite-Rauh2019a&gt;&lt;/a&gt;&lt;a name=cite-Hughes2019&gt;&lt;/a&gt;&lt;a name=cite-Peng2006a&gt;&lt;/a&gt;&lt;a name=cite-Seibold2021&gt;&lt;/a&gt;[[2](https://doi.org/10.1007/s11306-017-1299-3); [5](https://doi.org/10.1101/763730); [6](https://doi.org/10.1101/773473); [7](https://doi.org/10.1101/773945); [8](https://doi.org/10.1101/779702); [9](https://doi.org/10.1093/aje/kwj093); [10](https://doi.org/10.1371/journal.pone.0251194)]

## How can we check reproducibility if no code is given?

.footnotes[
Possible role models as research groups: 
[Jeff Leek](http://jtleek.com/codedata.html) and
[Ben Marwick](https://faculty.washington.edu/bmarwick/#publications).
]

???

But that doesn't even matter, because we can't have reproducibility if no one shares
their code!

And this is no joke. Getting data on this is difficult, but the research that has
been done shows that almost no one is sharing their code. The estimates range 
between fields in health science from zero to maybe five percent of published
studies. The only area that is doing pretty well is bioinformatics, at about 60%
of published studies.

We as researchers try to find our niche, make our own space in the research world.
Sometimes its a real struggle to find that niche.. but guys! No one is doing this,
no one is sharing their code! You start doing the simplest thing of sharing your
code and you will be one of very very few people who do. And this isn't a niche,
this is a gaping hole in our modern scientific process. A huge hole.

---

# Not going to lie, there are very strong...

## Institutional barriers

--

- Lack of adequate awareness, support, infrastructure, training

--

- Research culture values publications over all else

--

- More traditional academics don't understand or resist change

--

- 'Business as usual' is easier

???

You will encounter a lot of resistance, a lot of barriers and hardship.

At the institutional level, there is no real awareness of this,
no support or infrastructure. You're basically doing this on your own.
Which probably isn't that uncommon anyway.

Research culture and incentives pretty much only care about publishing journal
articles. Creating software tools, meh. Making teaching materials to help other
researchers, meh. Communicating your science to the public, meh. Doing
actual science that might take years and not lead to any "hard papers", meh.

We have a large portion of traditional academics who have benefitted from and
succeeded in this system and are invested in continuing it. Probably because they
don't understand the scope of the problem or just resist change.

We have a system that favours each individual person repeating the same mistakes
that others make because the system doesn't allow for us to take the time to
create tools and infrastructure that helps ourselves and others out.

Because business as usual is the easiest way in the short term. Our current
scientific culture is just not prepared for this, for the rising modern analytic
and computational era.

---

## ...and personal barriers

- Fear of:
    - Fear of being scooped or ideas being stolen
    - Not being credited for ideas
    - Errors and public humiliation
    - Risk to reputation

--

- Need to constantly stay updated

- Finding better opportunities outside of academia

.footnote[[Tennant (2017)](https://doi.org/10.6084/m9.figshare.5383711.v1)]

???

And there aren't just institutional barriers. We as researchers have fears of 
being scooped, of embarrassment and humiliation for your methods being *gasp* wrong.
Which is actually just part of science.

You also have to constantly stay updated, and that can be tiring.

And the last barrier, which may actually be a benefit, is that one reason you
don't see a lot of researchers sharing their code or being more reproducible
is.. they end up getting picked up by industry and paid really well or decide
to leave academia for the reasons I mentioned. 

Just as an example, I found a Norwegian group who had a really inefficient 
workflow and decided to re-build their workflow to make use of programming,
to be reproducible, to have a pipeline. I looked up the lead author as well as 
several other of the co-authors and guess what... many of them now work in really
great companies as data scientists or software engineers, probably making a lot 
of money and having potentially a less stressful life.

---

class: middle, center

# So... what you can do right now?

## Easiest thing: Start sharing your code

???

If you do nothing else: share your code.

If its ugly, that's fine! The point is you start and that you get more
comfortable doing it until it becomes second nature to share and in the process,
your code gets better because you know someone might look at your code. 

---

## How do you share?

- [GitHub](https://github.com/)

- [Zenodo](https://zenodo.org/)

- [figshare](https://figshare.com/)

- [Open Science Framework](https://osf.io/)

???

How do you share? Put your code up on any of these sites. I prefer a combination
of GitHub and Zenodo, but the others are also quite good as well.

When do you share? I say right away. As soon as I have an analysis project, 
my code is up on either GitHub or GitLab (another service like GitHub).
Alternatively, you can upload it when you also finish your manuscript.

---

## What else can you do?

- Find or start building a community of people using R

- Start doing code reviews within your group

- Start new projects or collaborations by:
    - Using R, R Markdown, Git, and GitHub
    - Aiming to share and be reproducible

.footnote[Code reviews: Reviewing each others code like you would review a manuscript.]
    
???

The other things you can start doing is find or start building a community of
people who also use R or are doing reproducibility or any other computational
work. Use them as support and help and also give back too.

Start doing code reviews in your research group. Code review would be where you
look over each others code, check that it works, check that it makes sense, 
that it's readable and understandable. The nice thing with doing code reviews
is that it dispels the mystery around code and about criticising it and trying
to improve it. We review manuscripts, why not code?
I personally though have had a really hard time getting groups I've been part of
now and in the past to do this, but baby steps.

---

class: middle, center

## And teaching others is a great way to learn 😉

???

Lastly.. you can teach. Teach others. Use these teaching materials. Or get involved
with this course next year. Or now. Actually, several participants from the beginner
course are or will soon be helping to improve the material for next year. 
There are also so many other things you can get involved in, aside from this course.
Let me know if you're interested!

---

# References, 1

&lt;a name=bib-Leek2017a&gt;&lt;/a&gt;[[1]](#cite-Leek2017a) J. T.
Leek and L. R. Jager. "Is Most Published Research
Really False?" In: _Annual Review of Statistics and
Its Application_ 4.1 (Mar. 2017), pp. 109-122. DOI:
[10.1146/annurev-statistics-060116-054104](https://doi.org/10.1146%2Fannurev-statistics-060116-054104).

&lt;a
name=bib-Considine2017a&gt;&lt;/a&gt;[[2]](#cite-Considine2017a)
E. C. Considine, G. Thomas, et al. "Critical Review of
Reporting of the Data Analysis Step in Metabolomics".
In: _Metabolomics_ 14.1 (Dec. 2017). DOI:
[10.1007/s11306-017-1299-3](https://doi.org/10.1007%2Fs11306-017-1299-3).

&lt;a name=bib-Plesser2018a&gt;&lt;/a&gt;[[3]](#cite-Plesser2018a)
H. E. Plesser. "Reproducibility Vs. Replicability: A
Brief History of a Confused Terminology". In:
_Frontiers in Neuroinformatics_ 11 (Jan. 2018). DOI:
[10.3389/fninf.2017.00076](https://doi.org/10.3389%2Ffninf.2017.00076).

&lt;a name=bib-Gil2016&gt;&lt;/a&gt;[[4]](#cite-Gil2016) Y. Gil,
C. H. David, et al. "Toward the Geoscience Paper of
the Future: Best practices for documenting and sharing
research from data to software to provenance". In:
_Earth and Space Science_ 3.10 (Oct. 2016), pp.
388-415. DOI:
[10.1002/2015ea000136](https://doi.org/10.1002%2F2015ea000136).

&lt;a name=bib-Rauh2019&gt;&lt;/a&gt;[[5]](#cite-Rauh2019) S.
Rauh, T. Torgerson, et al. "Reproducible and
Transparent Research Practices in Published Neurology
Research". In: _bioRxiv_ (Sep. 2019). DOI:
[10.1101/763730](https://doi.org/10.1101%2F763730).

---

# References, 2

&lt;a name=bib-Evans2019&gt;&lt;/a&gt;[[6]](#cite-Evans2019) S.
Evans, I. A. Fladie, et al. "Evaluation of
Reproducible and Transparent Research Practices in
Sports Medicine Research: A Cross-sectional study".
In: _bioRxiv_ (Sep. 2019). DOI:
[10.1101/773473](https://doi.org/10.1101%2F773473).

&lt;a name=bib-Rauh2019a&gt;&lt;/a&gt;[[7]](#cite-Rauh2019a) S. L.
Rauh, B. S. Johnson, et al. "Evaluation of
Reproducibility in Urology Publications". In:
_bioRxiv_ (Sep. 2019). DOI:
[10.1101/773945](https://doi.org/10.1101%2F773945).

&lt;a name=bib-Hughes2019&gt;&lt;/a&gt;[[8]](#cite-Hughes2019) T.
Hughes, A. Niemann, et al. "Transparent and
Reproducible Research Practices in the Surgical
Literature". In: _bioRxiv_ (Oct. 2019). DOI:
[10.1101/779702](https://doi.org/10.1101%2F779702).

&lt;a name=bib-Peng2006a&gt;&lt;/a&gt;[[9]](#cite-Peng2006a) R. D.
Peng, F. Dominici, et al. "Reproducible Epidemiologic
Research". In: _American Journal of Epidemiology_
163.9 (Mar. 2006), pp. 783-789. DOI:
[10.1093/aje/kwj093](https://doi.org/10.1093%2Faje%2Fkwj093).

&lt;a name=bib-Seibold2021&gt;&lt;/a&gt;[[10]](#cite-Seibold2021)
H. Seibold, S. Czerny, et al. "A computational
reproducibility study of PLOS ONE articles featuring
longitudinal data analyses". In: _PLOS ONE_ 16.6 (Jun.
2021). Ed. by J. M. Wicherts, p. e0251194. DOI:
[10.1371/journal.pone.0251194](https://doi.org/10.1371%2Fjournal.pone.0251194).
    </textarea>
<style data-target="print-only">@media screen {.remark-slide-container{display:block;}.remark-slide-scaler{box-shadow:none;}}</style>
<script src="https://remarkjs.com/downloads/remark-latest.min.js"></script>
<script>var slideshow = remark.create({
"ratio": "16:9",
"highlightStyle": "github",
"highlightLines": true,
"countIncrementalSlides": false
});
if (window.HTMLWidgets) slideshow.on('afterShowSlide', function (slide) {
  window.dispatchEvent(new Event('resize'));
});
(function(d) {
  var s = d.createElement("style"), r = d.querySelector(".remark-slide-scaler");
  if (!r) return;
  s.type = "text/css"; s.innerHTML = "@page {size: " + r.style.width + " " + r.style.height +"; }";
  d.head.appendChild(s);
})(document);

(function(d) {
  var el = d.getElementsByClassName("remark-slides-area");
  if (!el) return;
  var slide, slides = slideshow.getSlides(), els = el[0].children;
  for (var i = 1; i < slides.length; i++) {
    slide = slides[i];
    if (slide.properties.continued === "true" || slide.properties.count === "false") {
      els[i - 1].className += ' has-continuation';
    }
  }
  var s = d.createElement("style");
  s.type = "text/css"; s.innerHTML = "@media print { .has-continuation { display: none; } }";
  d.head.appendChild(s);
})(document);
// delete the temporary CSS (for displaying all slides initially) when the user
// starts to view slides
(function() {
  var deleted = false;
  slideshow.on('beforeShowSlide', function(slide) {
    if (deleted) return;
    var sheets = document.styleSheets, node;
    for (var i = 0; i < sheets.length; i++) {
      node = sheets[i].ownerNode;
      if (node.dataset["target"] !== "print-only") continue;
      node.parentNode.removeChild(node);
    }
    deleted = true;
  });
})();
// add `data-at-shortcutkeys` attribute to <body> to resolve conflicts with JAWS
// screen reader (see PR #262)
(function(d) {
  let res = {};
  d.querySelectorAll('.remark-help-content table tr').forEach(tr => {
    const t = tr.querySelector('td:nth-child(2)').innerText;
    tr.querySelectorAll('td:first-child .key').forEach(key => {
      const k = key.innerText;
      if (/^[a-z]$/.test(k)) res[k] = t;  // must be a single letter (key)
    });
  });
  d.body.setAttribute('data-at-shortcutkeys', JSON.stringify(res));
})(document);
(function() {
  "use strict"
  // Replace <script> tags in slides area to make them executable
  var scripts = document.querySelectorAll(
    '.remark-slides-area .remark-slide-container script'
  );
  if (!scripts.length) return;
  for (var i = 0; i < scripts.length; i++) {
    var s = document.createElement('script');
    var code = document.createTextNode(scripts[i].textContent);
    s.appendChild(code);
    var scriptAttrs = scripts[i].attributes;
    for (var j = 0; j < scriptAttrs.length; j++) {
      s.setAttribute(scriptAttrs[j].name, scriptAttrs[j].value);
    }
    scripts[i].parentElement.replaceChild(s, scripts[i]);
  }
})();
(function() {
  var links = document.getElementsByTagName('a');
  for (var i = 0; i < links.length; i++) {
    if (/^(https?:)?\/\//.test(links[i].getAttribute('href'))) {
      links[i].target = '_blank';
    }
  }
})();
// adds .remark-code-has-line-highlighted class to <pre> parent elements
// of code chunks containing highlighted lines with class .remark-code-line-highlighted
(function(d) {
  const hlines = d.querySelectorAll('.remark-code-line-highlighted');
  const preParents = [];
  const findPreParent = function(line, p = 0) {
    if (p > 1) return null; // traverse up no further than grandparent
    const el = line.parentElement;
    return el.tagName === "PRE" ? el : findPreParent(el, ++p);
  };

  for (let line of hlines) {
    let pre = findPreParent(line);
    if (pre && !preParents.includes(pre)) preParents.push(pre);
  }
  preParents.forEach(p => p.classList.add("remark-code-has-line-highlighted"));
})(document);</script>

<script>
slideshow._releaseMath = function(el) {
  var i, text, code, codes = el.getElementsByTagName('code');
  for (i = 0; i < codes.length;) {
    code = codes[i];
    if (code.parentNode.tagName !== 'PRE' && code.childElementCount === 0) {
      text = code.textContent;
      if (/^\\\((.|\s)+\\\)$/.test(text) || /^\\\[(.|\s)+\\\]$/.test(text) ||
          /^\$\$(.|\s)+\$\$$/.test(text) ||
          /^\\begin\{([^}]+)\}(.|\s)+\\end\{[^}]+\}$/.test(text)) {
        code.outerHTML = code.innerHTML;  // remove <code></code>
        continue;
      }
    }
    i++;
  }
};
slideshow._releaseMath(document);
</script>
<!-- dynamically load mathjax for compatibility with self-contained -->
<script>
(function () {
  var script = document.createElement('script');
  script.type = 'text/javascript';
  script.src  = 'https://mathjax.rstudio.com/latest/MathJax.js?config=TeX-MML-AM_CHTML';
  if (location.protocol !== 'file:' && /^https?:/.test(script.src))
    script.src  = script.src.replace(/^https?:/, '');
  document.getElementsByTagName('head')[0].appendChild(script);
})();
</script>
  </body>
</html>
